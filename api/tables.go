package api

import (
	"connection/errorHandler"
	"connection/repository"
	"encoding/json"
	"net/http"
)

// GetTables godoc
// @Summary Get all tables
// @Description Get all tables
// @Tags tables
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.Table
// @Router /api/tables [get]
func GetTables(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")

	columns := repository.GetAllTables()

	err := json.NewEncoder(w).Encode(columns)
	errorHandler.CheckError(err)
}
