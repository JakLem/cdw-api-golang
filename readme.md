This application needs two config, yaml files to works.

First is app.config.yaml which contains port.<br/>
<code>
port: 8010
</code>

Second file is db.config.yaml, this file contains database connection data.<br/>
<code>host: localhost<br/>
login: postgres<br/>
password: password<br/>
database: postgres<br/>
port: 5432</code>