package repository

import (
	"connection/entity"
	"connection/errorHandler"
	"connection/server"
	"database/sql"
)

func GetAllTables() []entity.Table {
	var tables []entity.Table
	results, err := server.Select("select * from db_all_tables")

	for results.Next() {
		var table entity.Table
		err = ScanTablesData(results, &table)
		errorHandler.CheckError(err)

		tables = append(tables, table)
	}

	return tables
}

func ScanTablesData(results *sql.Rows, table *entity.Table) error {
	return results.Scan(&table.Name)
}
