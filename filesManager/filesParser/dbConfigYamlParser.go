package filesParser

import (
	"connection/filesManager"
	"gopkg.in/yaml.v2"
	"log"
)

type DBConfig struct {
	Host     string `yaml:"host"`
	Login    string `yaml:"login"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
	Port     string `yaml:"port"`
}

func GetDataFromYaml(fileName string) DBConfig {
	config := DBConfig{}
	configContent := filesManager.GetData(fileName)

	err := yaml.Unmarshal([]byte(configContent), &config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return config
}
