package entity

type ViewColumns struct {
	ColumnName string `json:"columnName" example:"Name"`
	DataType   string `json:"dataType example:"Table""`
	Nullable   bool   `json:"nullable" example:Null`
}
