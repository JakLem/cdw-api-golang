package api

import (
	"connection/creator"
	"connection/refresher"
	"connection/repository"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

type View struct {
	Query string `json:"query"`
	Name  string `json:"name"`
}

// CreateViewInDatabase godoc
// @Summary Create materialized view in database
// @Description Create materialized view in database
// @Tags view
// @Accept  json
// @Produce  json
// @Success 200 {object} View
// @Router /api/view/create [post]
func CreateViewInDatabase(w http.ResponseWriter, r *http.Request) {
	var View View

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewDecoder(r.Body).Decode(&View)

	creator.CreateView(View.Name, View.Query)

	json.NewEncoder(w).Encode("success")
}

// RefreshView godoc
// @Summary Refreshing view with new query from body.
// @Description Refreshing view with new query from body.
// @Tags view
// @Accept  json
// @Produce  json
// @Success 200 {object} View
// @Router /api/views/refresh [post]
func RefreshView(w http.ResponseWriter, r *http.Request) {
	var View View

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewDecoder(r.Body).Decode(&View)

	refresher.RefreshMaterializedView(View.Name)

	json.NewEncoder(w).Encode("success")
}

func GetMaterializedViewData(w http.ResponseWriter, r *http.Request) {
	var ViewName = GetViewNameFromRequestBody(r)
	w.Header().Set("Content-Type", "application/json")

	/*viewData := */
	repository.GetMaterializedViewData(ViewName)

}

func GetViewNameFromRequestBody(r *http.Request) string {
	return mux.Vars(r)["view"]
}
