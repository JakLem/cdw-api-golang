package api

import (
	"connection/errorHandler"
	"connection/repository"
	"encoding/json"
	"fmt"
	"net/http"
)

// GetColumnsForView godoc
// @Summary Get all columns for specific view.
// @Description Get all columns for specific view.
// @Tags view
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.ViewColumns
// @Router /api/views/{view}/columns [get]
func GetColumnsForView(w http.ResponseWriter, r *http.Request) {
	var viewName string = GetViewNameFromRequestBody(r)
	columns := repository.GetColumnsForView(viewName)

	fmt.Println(viewName)

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(columns)
	errorHandler.CheckError(err)
}
