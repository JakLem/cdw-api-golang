package repository

import (
	"connection/entity"
	"connection/errorHandler"
	"connection/server"
	"database/sql"
	"fmt"
)

func GetColumnsForView(viewName string) []entity.ViewColumns {
	var viewColumns []entity.ViewColumns
	results, err := server.Select(fmt.Sprintf("SELECT a.attname as columnName, pg_catalog.format_type(a.atttypid, a.atttypmod) as type, a.attnotnull as nullable FROM pg_attribute a JOIN pg_class t on a.attrelid = t.oid JOIN pg_namespace s on t.relnamespace = s.oid WHERE a.attnum > 0 AND NOT a.attisdropped AND t.relname = '%s' AND s.nspname = 'public' ORDER BY a.attnum;", viewName))
	for results.Next() {
		var viewColumn entity.ViewColumns

		err = ScanViewWithColumnsData(results, &viewColumn)
		errorHandler.CheckError(err)

		viewColumns = append(viewColumns, viewColumn)
	}
	return viewColumns
}

func ScanViewWithColumnsData(results *sql.Rows, viewColumns *entity.ViewColumns) error {
	return results.Scan(&viewColumns.ColumnName, &viewColumns.DataType, &viewColumns.Nullable)
}
