package api

import (
	"connection/errorHandler"
	"connection/repository"
	"encoding/json"
	"net/http"
)

// GetAllMaterializedViews godoc
// @Summary Get all materialized views
// @Description Get all materialized views
// @Tags view
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.MaterializedViewStructure
// @Router /api/views [get]
func GetAllMaterializedViews(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")

	views := repository.GetAllMaterializedViews()

	err := json.NewEncoder(w).Encode(views)
	errorHandler.CheckError(err)
}
