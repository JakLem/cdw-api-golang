package repository

import (
	"connection/entity"
	"connection/errorHandler"
	"connection/server"
	"database/sql"
)

func GetAllMaterializedViews() []entity.MaterializedViewStructure {
	var views []entity.MaterializedViewStructure
	results, err := server.Select("select viewname from db_all_materialized_views")

	for results.Next() {
		var view entity.MaterializedViewStructure
		err = ScanViewData(results, &view)
		errorHandler.CheckError(err)
		views = append(views, view)
	}

	return views
}

func ScanViewData(results *sql.Rows, view *entity.MaterializedViewStructure) error {
	return results.Scan(&view.ViewName)
}
