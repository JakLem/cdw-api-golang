package repository

import "fmt"

func GetMaterializedViewData(name string) /*[]map[string]interface{}*/ {
	ViewFields := BuildViewStructDynamically(name)

	fmt.Println(ViewFields)

	//results, _ := server.Select(fmt.Sprintf("select * from %s", name))

	/*	for results.Next() {
			ScanViewColumnData(results,)
			//errorHandler.CheckError(err)
			ViewFields = append(ViewFields, view)
		}
	*/
	//return ViewFields
}

func BuildViewStructDynamically(name string) map[string]interface{} {
	ViewField := map[string]interface{}{}
	columns := GetColumnsForView(name)

	for _, element := range columns {
		ViewField[element.ColumnName] = element.DataType
	}

	fmt.Println(columns)
	fmt.Println(ViewField["name"])
	return ViewField
}
