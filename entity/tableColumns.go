package entity

type TableColumns struct {
	ColumnName string `json:"columnName" example:"Name"`
	DataType   string `json:"dataType" example:"Type"`
}
