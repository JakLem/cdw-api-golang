package filesManager

import (
	"io/ioutil"
	"log"
)

func GetData(fileName string) string {
	fileContent, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}

	return string(fileContent)
}
