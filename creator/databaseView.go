package creator

import (
	"connection/server"
	"fmt"
)

func CreateView(Name string, Query string) {
	Query = fmt.Sprintf("CREATE MATERIALIZED VIEW %s as %s", Name, Query)

	server.CreateMaterializedView(Query)
}
