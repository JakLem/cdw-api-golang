package refresher

import (
	"connection/server"
	"fmt"
)

func RefreshMaterializedView(name string) {
	query := fmt.Sprintf("REFRESH MATERIALIZED VIEW %s", name)

	server.ExecuteQuery(query)
}
