package repository

import (
	"connection/entity"
	"connection/errorHandler"
	"connection/server"
	"database/sql"
	"fmt"
)

func GetColumnsForTable(tableName string) []entity.TableColumns {
	var tablesColumns []entity.TableColumns
	results, err := server.Select(fmt.Sprintf("select t.ColumnName, t.DataType from db_tables_with_columns t where TableName = '%s'", tableName))
	for results.Next() {
		var tableColumn entity.TableColumns

		err = ScanTablesWithColumnsData(results, &tableColumn)
		errorHandler.CheckError(err)

		tablesColumns = append(tablesColumns, tableColumn)
	}

	return tablesColumns
}

func ScanTablesWithColumnsData(results *sql.Rows, tableColumns *entity.TableColumns) error {
	return results.Scan(&tableColumns.ColumnName, &tableColumns.DataType)
}

func ScanTableWithColumnData(result *sql.Row, tableColumns *entity.TableColumns) error {
	return result.Scan(&tableColumns.ColumnName, &tableColumns.DataType)
}
