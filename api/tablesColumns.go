package api

import (
	"connection/errorHandler"
	"connection/repository"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

// GetColumnsForTable godoc
// @Summary Get columns for specific table
// @Description Get columns for specific table
// @Tags tables
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.TableColumns
// @Router /api/tables/{table}/columns [get]
func GetColumnsForTable(w http.ResponseWriter, r *http.Request) {
	var tableName string = GetTableNameFromRequestBody(r)
	columns := repository.GetColumnsForTable(tableName)

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(columns)
	errorHandler.CheckError(err)
}

func GetTableNameFromRequestBody(r *http.Request) string {
	return mux.Vars(r)["table"]
}
