package server

import (
	"connection/singleton"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"strings"
)

var connector *sql.DB

func CreateMaterializedView(query string) {
	connector = singleton.OpenConnection("postgresql")
	query = strings.ToLower(query)
	if !strings.Contains(query, "create materialized view") {
		panic("Can't use CREATE MATERIALIZED VIEW query to run different query")
	}

	connector.Exec(query)
}

func ExecuteQuery(query string) {
	connector = singleton.OpenConnection("postgresql")

	connector.Exec(query)
}

func SelectSingleRow(query string) *sql.Row {
	connector = singleton.OpenConnection("postgresql")
	query = strings.ToLower(query)
	if !strings.Contains(query, "select") {
		panic("Can't use select query to run different query")
	}

	result := connector.QueryRow(query)

	return result
}

func Select(query string) (*sql.Rows, error) {
	connector = singleton.OpenConnection("postgresql")
	query = strings.ToLower(query)
	if !strings.Contains(query, "select") {
		panic("Can't use select query to run different query")
	}

	results, err := connector.Query(query)

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	return results, err
}
