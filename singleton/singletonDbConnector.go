package singleton

import (
	"connection/filesManager/filesParser"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"sync"
)

var connector *sql.DB
var once sync.Once

func OpenConnection(Connection string) *sql.DB {
	once.Do(func() {
		fmt.Printf("Creating Single Instance For %s Now\n", Connection)
		switch Connection {
		case "mysql":
			StartMySqlConnection()
			break
		case "postgresql":
			StartPostgreSqlConnection()
			break
		}
	})

	if err := connector.Ping(); err != nil {
		connector.Close()
		fmt.Println(err)
	}

	return connector
}

func StartMySqlConnection() {
	config := GetDBLoginData()
	var err error
	connector, err = sql.Open("mysql", fmt.Sprintf("%s:%s@/%s", config.Login, config.Password, config.Database))

	if err != nil {
		panic(err)
	}
}

func StartPostgreSqlConnection() {
	config := GetDBLoginData()
	var err error

	connector, err = sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", config.Login, config.Password, config.Host, config.Database))

	if err != nil {
		panic(err)
	}
}

func GetDBLoginData() filesParser.DBConfig {
	config := filesParser.GetDataFromYaml("db.config.yaml")
	fmt.Sprint(config)
	return config
}
