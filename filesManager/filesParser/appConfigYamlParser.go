package filesParser

import (
	"connection/filesManager"
	"gopkg.in/yaml.v2"
	"log"
)

type AppConfig struct {
	Port string `yaml:"port"`
}

func GetDataFromAppConfigYaml(fileName string) AppConfig {
	config := AppConfig{}
	configContent := filesManager.GetData(fileName)

	err := yaml.Unmarshal([]byte(configContent), &config)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return config
}
